# OpenML dataset: The-Price-and-Sales-of-Avocado

https://www.openml.org/d/43659

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The database update for Justin (https://www.kaggle.com/neuromusic/avocado-prices) 
From a BIG Fan of Avocado Toast
Content
This data was downloaded from the Hass Avocado Board website in January of 2020. 
Columns in the dataset:
  Date - The date of the observation

  AveragePrice - The Average Sales Price of Current Year

  Total Volume - Total Bulk and Bags Units

  4046 - Total number of avocados with PLU 4046 sold

  4225 - Total number of avocados with PLU 4225 sold

  4770 - Total number of avocados with PLU 4770 sold

  type - conventional or organic

  year - the current year

  region - the city or region of the observation

Acknowledgements
Thanks again to the Hass Avocado Board for sharing this data and thanks to Justin for share the idea about Avocado.
http://www.hassavocadoboard.com/retail/volume-and-price-data

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43659) of an [OpenML dataset](https://www.openml.org/d/43659). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43659/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43659/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43659/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

